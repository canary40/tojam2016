﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Lazer : MonoBehaviour {

    //CONSTANTS
    private const float LIFE_END_THRESHOLD = 1.25f;

    //COMPONENT REFERENCES
    private LineRenderer Renderer;

    //PRIVATE VARIABLES
    //Work Variables
    private Vector3[] LazerPoints;
    private bool SurfaceHit = false;
    private float Elapsedtime;

    //Lazer Travel Variables
    private float LazerStepTimer;
    private float LazerStepSize;
    private int LazerPointCount;

    //PUBLIC FIELDS
    public float LazerSpeed; //in units per second
    public float LazerLength;
    public Vector3 LazerDirection; /*{ get; set; }*/
    public float LifeTimer;


    // Use this for initialization
    void Start () {
        //Compute Lazer Parameters based on inspector attributes
        LazerStepSize = 3.0f;
        LazerStepTimer = 0.0001f;
        LazerPointCount = (int)(LazerLength / LazerStepSize);
        LazerStepTimer = LazerStepSize / LazerSpeed;

        //Initialize lazer rendering components
        Renderer = this.GetComponent<LineRenderer>();
        Renderer.SetColors(Color.red, Color.red);
        Renderer.material = new Material(Shader.Find("Particles/Additive"));
        Renderer.SetWidth(0.4f, 0.4f);
        Renderer.SetVertexCount(LazerPointCount);
        LazerPoints = new Vector3[LazerPointCount];
        for (int i = 0; i < LazerPointCount; i++)
        {
            LazerPoints[i] = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        }
        Renderer.SetPositions(LazerPoints);

        //Lazer firing startup variables
        Elapsedtime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Elapsedtime >= LazerStepTimer)
        {
            List<Vector3> _lazerPoints = LazerPoints.Cast<Vector3>().ToList();
            if (!SurfaceHit)
            {
                //Step all points forward
                for (int i = LazerPointCount - 1; i > 0; i--)
                {
                    _lazerPoints[i] = LazerPoints[i - 1];
                }
                _lazerPoints[0] = LazerPoints[0] + LazerDirection * LazerStepSize;

                //Check for collisions with anything along the lazer by casting a ray along the front few links
                for (int i = 1; i < LazerPointCount; i++)
                {
                    RaycastHit _hit;
                    Physics.Raycast(_lazerPoints[i], LazerDirection, out _hit, LazerStepSize + 0.03f);
                    if (_hit.collider != null)
                    {
                        //adjust all the points forward so that the lazer segment ends line up with the collision surface
                        for (int j = 0; j < LazerPointCount; j++)
                        {
                            _lazerPoints[j] = _lazerPoints[j] + _hit.point - _lazerPoints[i];
                        }

                        //Remove all lazer points beyond the mirror and reduce the lazer point count
                        for (int j = 0; j < i; j++)
                        {
                            _lazerPoints.RemoveAt(j);
                        }
                        LazerPointCount = LazerPointCount - i;

                        //if you hit a mirror tell it to spawn a new lazer at the angle of reflection
                        if (_hit.collider.gameObject.GetComponent<Reflector>() != null)
                        {
                            var r = Vector3.Reflect(LazerDirection, _hit.normal);
                            var delta = r - _hit.point;
                            var angle = Mathf.Atan2(delta.y, delta.x);
                            _hit.collider.gameObject.GetComponent<Reflector>().Reflect(_hit.point, r, LazerDirection, LazerSpeed, LazerLength, this.transform.rotation, LifeTimer);
                        }
                        else if (_hit.collider.gameObject.GetComponentInParent<CarControl>() != null)
                        {
                            _hit.collider.gameObject.GetComponentInParent<CarControl>().Kill();
                        }
                        SurfaceHit = true;
                        break;
                    }
                }
            }
            else
            {
                //Step all points forward
                if (LazerPointCount > 0)
                {
                    for (int i = LazerPointCount - 1; i > 0; i--)
                    {
                        _lazerPoints[i] = LazerPoints[i - 1];
                    }
                    _lazerPoints.RemoveAt(0);
                    LazerPointCount--;
                }
                else
                {
                    Destroy(this.gameObject);
                }
            }

            LazerPoints = _lazerPoints.ToArray();
            Renderer.SetVertexCount(LazerPointCount);
            Renderer.SetPositions(LazerPoints);
            Elapsedtime = 0.0f;
        }
        Elapsedtime = Elapsedtime + Time.deltaTime;
        LifeTimer = LifeTimer + Time.deltaTime;
        if (LifeTimer >= LIFE_END_THRESHOLD)
        {
            SurfaceHit = true;
        }
    }
}

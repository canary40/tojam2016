﻿using UnityEngine;
using System.Collections;

public class TimedLife : MonoBehaviour {
    float elapsedTime = 0;
    public float maxLife;
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void FixedUpdate() {
        if (elapsedTime > maxLife) {
            Destroy(gameObject);
        }
        elapsedTime += Time.deltaTime;
    }
}

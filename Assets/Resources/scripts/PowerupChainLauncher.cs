﻿using UnityEngine;
using System.Collections;
using System;

public class PowerupChainLauncher : MonoBehaviour {
    public GameObject car;
    public GameObject chaintip;
    public float shootCooldown;
    public float anchoringTime;
    float timeToShoot;

    void Start() {
        timeToShoot = shootCooldown;
    }

    void FixedUpdate() {
        if (timeToShoot > 0) {
            timeToShoot -= Time.deltaTime;
        }
    }

    public void Use(Vector3 source, Quaternion direction) {
        if (timeToShoot > 0) {
            return;
        }
        timeToShoot = shootCooldown;
        chaintip = GameObject.Instantiate(Resources.Load("prefabs/chaintip2"), source + car.transform.forward*1.5f, car.transform.rotation) as GameObject;
        Physics.IgnoreCollision(chaintip.GetComponent<Collider>(), car.GetComponent<Collider>());
        var ct = chaintip.GetComponent<ChainTip2>();
        ct.chainend = car;
        ct.lifetime = anchoringTime;
        var body = chaintip.GetComponent<Rigidbody>();
        body.velocity = car.GetComponent<Rigidbody>().velocity;
        body.AddRelativeForce(Vector3.forward* 16000);
    }

    public void UseUp(Vector3 source, Quaternion direction) {
        chaintip.GetComponent<ChainTip2>().Release();
    }
}

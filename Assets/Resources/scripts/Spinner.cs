﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {
    public float rate;
    // Use this for initialization
    // Update is called once per frame
    void Update () {
        transform.RotateAroundLocal(Vector3.up, rate);
    }
}

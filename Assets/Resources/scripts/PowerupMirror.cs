﻿using UnityEngine;
using System.Collections;

public class PowerupMirror : MonoBehaviour {

    private Vector3 LocalTransform;

    // Use this for initialization
    void Start () {
        LocalTransform = (2.5f * this.transform.forward) + Vector3.up*1.75f;
        this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    // Update is called once per frame
    void Update () {
        this.transform.position = this.transform.parent.position + LocalTransform;
        this.transform.forward = new Vector3(LocalTransform.x, 0, LocalTransform.z);
    }

    public void UpdatePosition(float _thumbX, float _thumbY) {
        if(new Vector3(_thumbX, 0, _thumbY).magnitude > 0.7f)
        {
            LocalTransform = 2.5f * (new Vector3(_thumbX, 0, _thumbY).normalized) + new Vector3(0, 1.75f, 0);
        }
        //transform.localPosition = new Vector3(Mathf.Cos(rotation), transform.position.y, Mathf.Sin(rotation));
        //transform.localRotation = Quaternion.Euler(0, Mathf.Rad2Deg*(rotation - Mathf.PI*0.5f), 0);
        //transform.forward = transform.parent.position - transform.position;
    }
}

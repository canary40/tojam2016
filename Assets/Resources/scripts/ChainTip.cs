﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChainTip : MonoBehaviour {
    Joint joint;
    static float THRESHOLD_CHAIN_PROXIMITY = .5f;
    static float MAX_CHAIN_LENGTH = 16f;
    public List<GameObject> links;
    public GameObject chainend;
    public HingeJoint sourceJoint;
    bool joined = false;
    bool releaseOnContact = false;
    bool released = false;
    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
        if (!released && (transform.position-chainend.transform.position).magnitude > MAX_CHAIN_LENGTH) {
            Kill();
        }
    }

    void Kill() {
        foreach (var l in links) {
            Destroy(l);
        }
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.collider.gameObject != chainend.gameObject && !joined) {
            joined = true;
            var fj = gameObject.AddComponent<FixedJoint>();
            fj.connectedBody = collision.rigidbody;
            BuildChain();
            sourceJoint.connectedBody = null;
            released = true;
            GetComponent<Collider>().isTrigger = true;
        }
    }
    public void Attach(Joint j) {
        joint = j;
    }
    public void Release() {
    }
    public void BuildChain() {
        Vector3 start = transform.position;
        Vector3 end = chainend.transform.position;
        var delta = end - start;
        var deltanorm = delta.normalized;
        var length = delta.magnitude;
        GameObject link = null;
        Rigidbody prevLink = null;
        int i = 0;
        links = new List<GameObject>();
        while (true) {
            link = GameObject.Instantiate(Resources.Load("prefabs/link"), start + deltanorm * 2.0f * i++, Quaternion.FromToRotation(Vector3.forward, deltanorm)) as GameObject;
            links.Add(link);
            var linkcmp = link.GetComponent<Link>();
            linkcmp.Init();
            linkcmp.front.anchor = new Vector3(0, 0, -1.2f);
            if (prevLink != null) {
                linkcmp.front.connectedBody = prevLink;
            } else {
                linkcmp.front.connectedBody = GetComponent<Rigidbody>();
                GetComponent<ChainTip>().Attach(linkcmp.front);
            }
            prevLink = link.GetComponent<Rigidbody>();
            if ((link.transform.position - chainend.transform.position).magnitude < THRESHOLD_CHAIN_PROXIMITY) {
                sourceJoint = link.AddComponent<HingeJoint>();
                sourceJoint.anchor = new Vector3(0, 0, 1.2f);
                sourceJoint.connectedBody = chainend.GetComponent<Rigidbody>();
                break;
            }
        }
    }
}

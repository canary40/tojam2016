﻿using UnityEngine;
using System.Collections;

public class Levitator : MonoBehaviour {
    public float heightToRaise;
    public float timeToRaise;
    float initialHeight;
    float elapsedTime = 0;
    int state = 0; // 0 1 2 idle raising sinking
    bool raised = false;
    // Use this for initialization
    void Start () {
        initialHeight = transform.position.y;
    }

    // Update is called once per frame
    void Update () {
        elapsedTime += Time.deltaTime;
        if (state == 1) {
            transform.position = new Vector3(transform.position.x,initialHeight + heightToRaise * elapsedTime / timeToRaise,transform.position.z);
            if (elapsedTime > timeToRaise) {
                state = 0;
                raised = true;
            }
        } else if (state == 2) {
            transform.position = new Vector3(transform.position.x, initialHeight + heightToRaise * (1- elapsedTime / timeToRaise), transform.position.z);
            if (elapsedTime > timeToRaise) {
                state = 0;
                raised = false;
            }
        }
    }
    public void Raise() {
        if (raised) return;
        state = 1;
        elapsedTime = 0;
    }
    public void Sink() {
        if (!raised) return;
        state = 2;
        elapsedTime = 0;
    }
}

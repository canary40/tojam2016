﻿using UnityEngine;
using System.Collections;

public class LazerController : MonoBehaviour {

    //PRIVATE VARIABLES
    LazerFiring[] LazerSources;
    private int PatternIndex;
    private int PatternCycle;
    public int PatternLength;
    private float Timer;
    private float PeriodTimer;

    //PUBLIC VARIABLES
    public float FiringPeriod = 0.75f;

    //PATTERNS
    private bool PatternInProgress = false;
    public uint[][] Patterns = { new uint[] { 1, 2, 4, 8, 16, 32, 64, 128 }, //one at a time around
                                  new uint[]{ 4 | 64, 1 | 16, 2 | 32, 8 | 128 }, //pairs opposing
                                  new uint[]{ 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 }, //All at once (A.K.A. SUDDEN DEATH)
                                  new uint[]{ 1 | 2 | 4 | 8 , 16 | 32 | 64 | 128 },
                                  new uint[]{ 1 | 2,  4 | 8, 1 | 2, 16 | 32, 4 | 8, 64 | 128, 16 | 32, 64 | 128 }
                                };

    // Use this for initialization
    void Start () {
        Timer = 100000;
        PeriodTimer = 100000;
        LazerSources = this.gameObject.GetComponentsInChildren<LazerFiring>();
        PatternIndex = Random.Range(0, Patterns.Length);
        PatternCycle = 0;
        PatternLength = Patterns[PatternIndex].Length;
        TriggerPattern(0);
    }

    // Update is called once per frame
    void Update()
    {
        TriggerPattern(Random.Range(0, Patterns.Length));
        if (Timer <= PatternLength * FiringPeriod)
        {
            if (PeriodTimer <= FiringPeriod)
            {
                PeriodTimer = PeriodTimer + Time.deltaTime;
            }
            else
            {
                Fire(new BitArray(System.BitConverter.GetBytes(Patterns[PatternIndex][PatternCycle])));
                PatternCycle++;
                PeriodTimer = 0;
                if (PatternCycle < PatternLength - 1)
                {
                    Warn(new BitArray(System.BitConverter.GetBytes(Patterns[PatternIndex][PatternCycle])));
                }
            }
            Timer = Timer + Time.deltaTime;
        }
    }

    //
    public void TriggerPattern(int _patternIndex)
    {
        if (Timer >= PatternLength * FiringPeriod)
        {
            Timer = 0;
            PeriodTimer = 0;
            PatternIndex = _patternIndex;
            PatternCycle = 0;
            PatternLength = Patterns[PatternIndex].Length;
            Warn(new BitArray(System.BitConverter.GetBytes(Patterns[PatternIndex][PatternCycle])));
        }
    }

    //
    private void Fire(BitArray _lazers)
    {
        for (int i = 0; i < _lazers.Length; i++)
        {
            if (_lazers[i] == true)
            {
                LazerSources[i].Fire();
                Debug.Log(LazerSources[i].transform.right);
            }
        }
    }

    //
    private void Warn(BitArray _lazers)
    {
        for (int i = 0; i < _lazers.Length; i++)
        {
            if (_lazers[i] == true)
            {
                LazerSources[i].Warn();
            }
        }
    }
}

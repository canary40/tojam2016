﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class CarSpawn : MonoBehaviour {
    bool occupied = false;
    public GameObject SpawnPlayer(int player) {
        //if (occupied) return null;
        var p = GameObject.Instantiate(Resources.Load("prefabs/Car 1"), transform.position, Quaternion.identity) as GameObject;
        p.GetComponent<CarControl>().playerIndex= (XInputDotNetPure.PlayerIndex)player;
        p.GetComponentInChildren<MeshRenderer>().materials = new Material[1] { Resources.Load("Models/Materials/CAR"+(player+1)) as Material};
        occupied = true;
        return p;
    }
    void OnTriggerEnter(Collider other) {
        occupied = true;
    }

    void OnTriggerExit(Collider other) {
        occupied = false;
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
    public GameObject target;
    Camera macamera;
    // Use this for initialization
    void Start () {
        macamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update () {
        macamera.transform.LookAt(target.transform);
    }
}

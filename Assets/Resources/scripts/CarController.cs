﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using XInputDotNetPure; // Required in C#

public class CarController : MonoBehaviour {
    public List<AxleInfo> axleInfos;
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxSteeringAngle; // maximum steer angle the wheel can have

    PowerupChainLauncher launcher;
    PowerupMirror mirror;
    #region XInput State Variables
    public bool playerIndexSet = false;
    public PlayerIndex playerIndex = PlayerIndex.One;
    GamePadState state;
    GamePadState prevState;

    #endregion

    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider) {
        if (collider.transform.childCount == 0) {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    public void FixedUpdate() {
        prevState = state;
        state = GamePad.GetState(playerIndex);
        if (transform.position.y < -5f) {
            Kill();
        }

        if (Input.GetKeyDown(KeyCode.Z)) {
            launcher.Use(transform.position, transform.rotation);
        }

        if (state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A == ButtonState.Released)
            launcher.Use(transform.position, transform.rotation);
        //else if (state.Buttons.A == ButtonState.Released && prevState.Buttons.A == ButtonState.Pressed && item != null)
        //item.UseUp(transform.position, transform.rotation);

        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        //float motor = maxMotorTorque * state.ThumbSticks.Left.Y;
        //float steering = maxSteeringAngle * state.ThumbSticks.Left.X;

        var xaxis = state.ThumbSticks.Right.X;
        var yaxis = state.ThumbSticks.Right.Y;
        //mirror.UpdatePosition(xaxis, yaxis);

        foreach (AxleInfo axleInfo in axleInfos) {
            if (axleInfo.steering) {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor) {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
            // Test
            var direction = transform.rotation;
            var angle = Mathf.Deg2Rad * (direction.y);
            var facing = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle));
        }
    }

    public void Kill() {
        Destroy(gameObject);
    }

    void Start() {
        launcher = GetComponent<PowerupChainLauncher>();
        launcher.car = gameObject;
        //mirror = GetComponentInChildren<PowerupMirror>();
        //mirror.transform.position = this.transform.position + Vector3.forward;
        //mirror.transform.forward = Vector3.forward;
    }
}

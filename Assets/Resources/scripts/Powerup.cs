﻿using UnityEngine;
using System.Collections;

public abstract class Powerup {
    abstract public void Use(Vector3 source, Quaternion direction);
    abstract public void UseUp(Vector3 source, Quaternion direction);
}

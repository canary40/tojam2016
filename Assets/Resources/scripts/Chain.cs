﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chain : MonoBehaviour {
    static float THRESHOLD_CHAIN_PROXIMITY = .5f;
    public List<Link> links;
    public GameObject chaintip;
    public GameObject chainend;

}

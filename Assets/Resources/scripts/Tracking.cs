﻿using UnityEngine;
using System.Collections;

public class Tracking : MonoBehaviour {

    GameObject TrackedObject;
    private Vector3 TargetAngle;
    private Vector3 CurrentAngle;

    // Use this for initialization
    void Start () {
        TrackedObject = GameObject.FindGameObjectWithTag("Player");
        CurrentAngle = this.transform.forward;
    }

    // Update is called once per frame
    void Update () {
        TargetAngle = TrackedObject.transform.position - this.transform.position;
        CurrentAngle = this.transform.forward;

        CurrentAngle = new Vector3(0/*Mathf.LerpAngle(CurrentAngle.x, TargetAngle.x, Time.deltaTime)*/,
                                   Mathf.LerpAngle(CurrentAngle.x, TargetAngle.x, 0.03f),
                                   0/*Mathf.LerpAngle(CurrentAngle.z, TargetAngle.z, Time.deltaTime)*/);
        transform.Rotate(CurrentAngle);
    }
}

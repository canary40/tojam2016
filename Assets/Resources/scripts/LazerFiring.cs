﻿using UnityEngine;
using System.Collections;

public class LazerFiring : MonoBehaviour {

    //CONSTANTS
    public float WARNING_LENGTH = 0.5f;
    public float RESET_THRESHOLD = 20.0f;
    public bool AUTOFIRE = false;
    public float length = 9;


    //PREFABS
    public GameObject Lazer;

    //PRIVATE VARIABLES
    private Vector3 FiringDirection;
    private float NextCycleFiringTime;
    bool Fired = false;
    float ResetTimer;

    //Warning Variables
    private bool WarningInProgress = false;
    private float WarningTimer = 0.0f;


    //COMPONENT INSTANCES
    private MeshRenderer Renderer;

    // Use this for initialization
    void Start () {
        Renderer = this.GetComponentInChildren<MeshRenderer>();
        //Renderer.enabled = false;
        FiringDirection = this.transform.forward;
        ResetTimer = 0.0f;
        NextCycleFiringTime = Random.Range(0.0f, RESET_THRESHOLD);
    }

    // Update is called once per frame
    void Update () {
        FiringDirection = this.transform.right;
        if (AUTOFIRE)
        {
            if (ResetTimer >= RESET_THRESHOLD)
            {
                ResetTimer = 0.0f;
                Fired = false;
                NextCycleFiringTime = Random.Range(0.0f, RESET_THRESHOLD);
            }
            if (ResetTimer >= NextCycleFiringTime && Fired == false)
            {
                GameObject _lazer = Instantiate(Lazer,
                                                this.transform.position,
                                                Quaternion.identity) as GameObject;
                _lazer.GetComponent<Lazer>().LazerDirection = FiringDirection;
                _lazer.GetComponent<Lazer>().LazerLength = length;
                _lazer.GetComponent<Lazer>().LazerSpeed = 300.0f;
                _lazer.GetComponent<Lazer>().LifeTimer = 0.0f;
                Fired = true;
            }
            ResetTimer += Time.deltaTime;
        }

        //Warning Update
        if (WarningInProgress)
        {
            if (WarningTimer < WARNING_LENGTH)
            {
                Material _mat = new Material(Renderer.material);
                _mat.color = new Color(1, 0, 0, WarningTimer / WARNING_LENGTH);
                WarningTimer = WarningTimer + Time.deltaTime;
            }
            else
            {
                WarningInProgress = false;
            }
        }
    }


    //
    public void Fire()
    {
        GameObject _lazer = Instantiate(Lazer,
                                        this.transform.position,
                                        Quaternion.identity) as GameObject;
        _lazer.GetComponent<Lazer>().LazerDirection = FiringDirection;
        _lazer.GetComponent<Lazer>().LazerLength = 25.0f;
        _lazer.GetComponent<Lazer>().LazerSpeed = 300.0f;
        _lazer.GetComponent<Lazer>().LifeTimer = 0.0f;
    }

    //
    public void Warn()
    {
        WarningInProgress = true;
        WarningTimer = 0.0f;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class ChainTip2 : MonoBehaviour {
    Joint joint;
    static float THRESHOLD_CHAIN_PROXIMITY = .5f;
    static float MAX_CHAIN_LENGTH = 16f;
    public List<GameObject> links;
    public GameObject chainend;
    [HideInInspector]
    public float lifetime;
    bool joined = false;
    bool releaseOnContact = false;
    bool released = false;
    TimedLife tl;
    LineRenderer linerender;
    // Use this for initialization
    void Start () {
        Assert.IsTrue(chainend != null);
        linerender = GetComponent<LineRenderer>();
        linerender.enabled = true;
        tl = GetComponent<TimedLife>();
    }

    // Update is called once per frame
    void Update () {
        if (joint != null) {
            linerender.SetPosition(0, joint.transform.position);
            linerender.SetPosition(1, joint.connectedBody.position);
        } else {
            linerender.SetPosition(0, transform.position);
            linerender.SetPosition(1, chainend.transform.position);
        }
    }

    void Kill() {
        foreach (var l in links) {
            Destroy(l);
        }
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.collider.gameObject.tag == "ground") {
            Kill();
            return;
        }
        if (collision.collider.gameObject != chainend.gameObject && !joined) {
            GetComponent<Collider>().enabled = false;
            transform.position = chainend.transform.position;
            var cj = gameObject.AddComponent<ConfigurableJoint>();
            cj.xMotion = cj.yMotion = cj.zMotion = ConfigurableJointMotion.Limited;
            var sjls = new SoftJointLimitSpring() { spring = 1000000f, damper = 1 };
            cj.linearLimitSpring = sjls;
            var sjl = new SoftJointLimit() { limit = Mathf.Clamp((transform.position - collision.collider.transform.position).magnitude, 2, 7) };
            cj.linearLimit = sjl;
            cj.connectedBody = collision.collider.GetComponent<Rigidbody>();
            joint = cj;
            var fj = gameObject.AddComponent<FixedJoint>();
            fj.connectedBody = null;
            joined = true;
            tl.maxLife = lifetime;
        }
    }
    public void Attach(Joint j) {
        joint = j;
    }
    public void Release() {
    }
}

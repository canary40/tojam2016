﻿using UnityEngine;
using System.Collections;

public class Reflector : MonoBehaviour {

    //PREFABS
    public GameObject LazerPrefab;

    public void Reflect(Vector3 _point, Vector3 newAngle, Vector3 _incidence, float _speed, float _length, Quaternion _quat, float _oldLifeTimer)
    {
        GameObject _lazer = Instantiate(LazerPrefab,
                                        _point,
                                        _quat) as GameObject;
        _lazer.GetComponent<Lazer>().LazerDirection = newAngle;
        _lazer.GetComponent<Lazer>().LazerSpeed = _speed;
        _lazer.GetComponent<Lazer>().LazerLength = _length;
        _lazer.GetComponent<Lazer>().LifeTimer = _oldLifeTimer;
    }

    //// Use this for initialization
    //void Start () {

    //}

    //// Update is called once per frame
    //void Update () {

    //}
}

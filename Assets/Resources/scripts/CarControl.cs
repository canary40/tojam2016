﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using XInputDotNetPure;

public class CarControl : MonoBehaviour {

    //PREFABS
    public GameObject MirrorPrefab;

    //PREFAB INSTANCES
    private GameObject MirrorInstance;

    //INSPECTOR VARIABLES
    public List<AxleInfo> AxleInfos;
    public float MaxMotorTorque; // maximum torque the motor can apply to wheel
    public float MaxSteeringAngle; // maximum steer angle the wheel can have

    //COMPONENT REFERENCES
    PowerupChainLauncher Launcher;
    PowerupMirror Mirror;

    //INPUT STATE VARIABLES
    public bool playerIndexSet = false;
    public PlayerIndex playerIndex = PlayerIndex.One;
    GamePadState state;
    GamePadState prevState;
    Rigidbody rbody;
    public float boostCooldown;
    float boostTime;
    bool alive = true;

    private void ApplyLocalPositionToVisuals(WheelCollider _coll)
    {
        if (_coll.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = _coll.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        _coll.GetWorldPose(out position, out rotation);
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    // Use this for initialization
    void Start () {
        //Setup the mirror
        MirrorInstance = Instantiate(MirrorPrefab,
                                     this.transform.position + 2.5f * Vector3.forward + new Vector3(0, 1.75f, 0),
                                     Quaternion.identity) as GameObject;
        MirrorInstance.transform.parent = this.transform;
        Mirror = MirrorInstance.GetComponent<PowerupMirror>();

        //Setup the launcher
        Launcher = this.GetComponent<PowerupChainLauncher>();
        Launcher.car = this.gameObject;
        rbody = GetComponent<Rigidbody>();
        alive = true;
    }

    // Update is called once per frame
    void FixedUpdate () {
        var ps = GetComponentsInChildren<ParticleSystem>();
        foreach (var p in ps) {
            var e = p.emission;
            if (rbody.velocity.magnitude < 4) {
                e.enabled = false;
            } else {
                e.enabled = true;
            }
        }
        //Save the prvious gamepad state and set the new one
        prevState = state;
        state = GamePad.GetState(playerIndex);

        //Compare the two states to determine if launcher should be fired, fire if necessary
        if (state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A == ButtonState.Released)
        {
            Launcher.Use(transform.position, transform.rotation);
        }
        if (state.Buttons.Y == ButtonState.Pressed && prevState.Buttons.A == ButtonState.Released) {
            Kill();
        }

        if (state.Buttons.B == ButtonState.Pressed && boostTime < 0) {
            rbody.AddRelativeForce(Vector3.forward * 1000000);
            boostTime = boostCooldown;
        } else {
            boostTime -= Time.deltaTime;
        }



        //////Set the steering and acceleration variables
        float _motor;
        float _steering;
        //float _angle = Vector3.Angle(this.transform.forward, DesiredDirection);
        ////Absolute Steering
        //if (Mathf.Abs(_angle) > MaxSteeringAngle)
        //{
        //    _steering = MaxSteeringAngle * _angle/Mathf.Abs(_angle);
        //}
        //else
        //{
        //    _steering = MaxSteeringAngle * _angle/MaxSteeringAngle;
        //}
        //_motor = MaxMotorTorque * DesiredDirection.magnitude;


        //Left/Right Steering
        _motor = MaxMotorTorque * state.Triggers.Right - MaxMotorTorque * state.Triggers.Left;
        _steering = MaxSteeringAngle * state.ThumbSticks.Left.X;

        //Tell the mirror to update based on the stick input
        Mirror.UpdatePosition(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);

        if (state.Buttons.X == ButtonState.Pressed) {
            rbody.AddRelativeForce(Vector3.down * 100000);
        }
        //Update the axles
        foreach (AxleInfo axleInfo in AxleInfos) {
            if (axleInfo.steering) {
                axleInfo.leftWheel.steerAngle = _steering;
                axleInfo.rightWheel.steerAngle = _steering;
                if (state.Buttons.X == ButtonState.Pressed) {
                    axleInfo.leftWheel.brakeTorque = 0;
                    axleInfo.rightWheel.brakeTorque = 0;
                } else {
                    axleInfo.leftWheel.brakeTorque = 0f;
                    axleInfo.rightWheel.brakeTorque = 0f;
                }
            }
            if (axleInfo.motor) {
                axleInfo.leftWheel.motorTorque = _motor;
                axleInfo.rightWheel.motorTorque = _motor;
            }
        ApplyLocalPositionToVisuals(axleInfo.leftWheel);
        ApplyLocalPositionToVisuals(axleInfo.rightWheel);

            // Test
            //var direction = transform.rotation;
            //var angle = Mathf.Deg2Rad * (direction.y);
            //var facing = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle));
            //Debug.DrawLine(transform.position, transform.position + transform.forward);
        }
    }

    //
    public void Kill()
    {
        if (!alive) return;
        alive = false;
        Instantiate(Resources.Load("prefabs/Explosion"), transform.position, Quaternion.identity);
        GameObject.FindGameObjectWithTag("director").GetComponent<Director>().Died((int)playerIndex);
        Destroy(gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

[RequireComponent(typeof(Rigidbody))]
public class Link : MonoBehaviour {
    public Joint front;
    // Use this for initialization
    public void Init () {
        Joint joints = GetComponent<HingeJoint>() as Joint;
        front = joints;
    }

    // Update is called once per frame
    void Update () {

    }
}

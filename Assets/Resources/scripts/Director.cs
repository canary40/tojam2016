﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;
using UnityEngine.Assertions;
using System.Collections.Generic;

public class Director : MonoBehaviour {
    public GameObject[] players = new GameObject[4]{ null, null, null, null };
    public bool[] playerStartRequest = new bool[4] { false, false, false, false };
    public CarSpawn spawner;
    public float elapsedTime = 0;
    public List<Levitator> mirrors;
    public GameObject trackingLaser;
    public GameObject retroLaser;
    public int state = 1; // mirrors, idle, tracking
    static float PHASE_DURATION = 35;
    // Use this for initialization
    void Start () {
        spawner = GameObject.FindGameObjectWithTag("spawner").GetComponent<CarSpawn>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        for (int i = 0; i < 4; ++i) {
            if (GamePad.GetState((PlayerIndex)i).Buttons.A == ButtonState.Pressed) {
                playerStartRequest[i] = true;
            }
        }
        for (int i =0; i < 4; ++i) {
            if (players[i] == null && playerStartRequest[i]) {
                players[i] = spawner.SpawnPlayer(i);
                if (players[i] != null) playerStartRequest[i] = false;
            }
        }
        elapsedTime += Time.deltaTime;
        switch(state) {
        case 1:
            if (elapsedTime > PHASE_DURATION/3.0f) {
                state++;
                trackingLaser.GetComponent<Levitator>().Raise();
                elapsedTime = 0;
            }
            break;
        case 0: // mirrors
            if (elapsedTime > PHASE_DURATION) {
                state++;
                foreach (var m in mirrors) {
                    m.Sink();
                }
                retroLaser.GetComponent<Levitator>().Sink();
                elapsedTime = 0;
            }
            break;
        case 2:
            if (elapsedTime > PHASE_DURATION) {
                foreach (var m in mirrors) {
                    m.Raise();
                }
                state = 0;
                retroLaser.GetComponent<Levitator>().Raise();
                trackingLaser.GetComponent<Levitator>().Sink();
                elapsedTime = 0;
            }
            break;
        }

    }

    public void MirrorsTime() {

    }

    public void Died(int playerIndex) {
        Assert.IsTrue(playerIndex >= 0 && playerIndex < 4);
        players[playerIndex] = null;
    }
}
